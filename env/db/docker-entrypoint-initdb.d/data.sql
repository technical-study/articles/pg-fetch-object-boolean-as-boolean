CREATE TABLE participant
(
    "id"        SERIAL NOT NULL,
    "name"      VARCHAR,
    "can_drink" BOOLEAN
);

INSERT INTO participant
    ("name", "can_drink")
VALUES
    ('John', TRUE),
    ('Jane', FALSE);
