# pg-fetch-object-boolean-as-boolean

PHP の pg_fetch_object 関数を使って boolean のカラムを boolean で格納する方法

## 入れるデータ

| id  | name | can\_drink |
|:----|:-----|:-----------|
| 1   | John | true       |
| 2   | Jane | false      |

## 検証

以下の SQL を実行する。

```sql
SELECT * FROM participant;
```

`pg_fetch_object` 関数の結果取得に以下の 2 つのクラスを用いて比較する。

- `Participant1`
- `Participant2`

## 結果

### `Participant1` クラスで取得した場合

```text
John can drink!
Jane can drink!
```

boolean の部分が正しく取得されていない。

### `Participant2` クラスで取得した場合

```text
John can drink!
Jane cannot drink...
```

boolean のカラムを PHP 内で boolean として扱うには `__set` メソッドでプロパティに値をセットするときの動作を指定するとよい。

---

## 補足

`Participant3` クラスを用いてマジックメソッドとコンストラクタの実行順を比較すると、マジックメソッドが先に実行されている。

```text
string(14) "__set executed"
array(2) {
[0]=>
string(2) "id"
[1]=>
string(1) "1"
}
string(14) "__set executed"
array(2) {
[0]=>
string(4) "name"
[1]=>
string(4) "John"
}
string(14) "__set executed"
array(2) {
[0]=>
string(9) "can_drink"
[1]=>
string(1) "t"
}
string(20) "constructor executed"
string(14) "__set executed"
array(2) {
[0]=>
string(2) "id"
[1]=>
string(1) "2"
}
string(14) "__set executed"
array(2) {
[0]=>
string(4) "name"
[1]=>
string(4) "Jane"
}
string(14) "__set executed"
array(2) {
[0]=>
string(9) "can_drink"
[1]=>
string(1) "f"
}
string(20) "constructor executed"
```
