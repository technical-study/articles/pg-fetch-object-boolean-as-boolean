<?php

/**
 * プロパティが存在しない場合、__set 関数 (マジックメソッド) が実行される。
 */
class Participant3
{
    // public int $id;
    // public string $name;
    // public bool $can_drink;

    /**
     * こちらが後で実行される！
     */
    public function __construct()
    {
        var_dump('constructor executed');
    }

    /**
     * こちらが先に実行される！
     *
     * @param string $name
     * @param        $value
     *
     * @return void
     * @noinspection MagicMethodsValidityInspection
     */
    public function __set(string $name, $value): void
    {
        $this->{$name} = $value;
        var_dump('__set executed', func_get_args());
    }
}
