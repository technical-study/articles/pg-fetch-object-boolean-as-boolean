<?php

/**
 * boolean のカラムだけプロパティを直接定義せず、マジックメソッドで正しい値を埋め込むようにする。
 *
 * @property bool $can_drink
 */
class Participant2
{
    /**
     * boolean 値として認識させたいカラムをここに記述する。
     *
     * @var array|string[]
     */
    protected static array $booleanColumns = [
        'can_drink',
    ];

    /**
     * string や int など、キャストされても意味合いが変わらない場合は、このようにプロパティで記述する。
     *
     * @var int
     */
    public int $id;
    public string $name;
    // public bool $can_drink;

    /** @noinspection MagicMethodsValidityInspection */
    public function __set(string $name, $value): void
    {
        if (in_array($name, self::$booleanColumns, true)) {
            $this->{$name} = match ($value) {
                't' => true,
                default => false,
            };

            return;
        }

        $this->{$name} = $value;
    }
}
