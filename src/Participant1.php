<?php

/**
 * DB の定義に合わせてプロパティを設定した場合
 */
class Participant1
{
    public int $id;
    public string $name;
    public bool $can_drink;
}
