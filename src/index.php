<?php

require_once __DIR__ . '/Participant1.php';
require_once __DIR__ . '/Participant3.php';
require_once __DIR__ . '/Participant2.php';

$db_conn = pg_connect('host=db port=5432 dbname=root user=root password=root');

$qu = pg_query($db_conn, 'SELECT * FROM participant');
while ($data = pg_fetch_object($qu, null, Participant1::class)) {
    $name = $data->name;
    $canDrink = $data->can_drink;
    $message = $canDrink
        ? "$name can drink!"
        : "$name cannot drink...";
    echo $message, PHP_EOL;
}

$qu = pg_query($db_conn, 'SELECT * FROM participant');
while ($data = pg_fetch_object($qu, null, Participant2::class)) {
    $name = $data->name;
    $canDrink = $data->can_drink;
    $message = $canDrink
        ? "$name can drink!"
        : "$name cannot drink...";
    echo $message, PHP_EOL;
}

$qu = pg_query($db_conn, 'SELECT * FROM participant');
while ($data = pg_fetch_object($qu, null, Participant3::class)) {
    $name = $data->name;
    $canDrink = $data->can_drink;
    $message = $canDrink
        ? "$name can drink!"
        : "$name cannot drink...";
    // echo $message, PHP_EOL;
}
